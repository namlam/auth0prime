import { Button } from "primereact/button";
import React from "react";
import { getLocalStorage } from "../../../../utils/storage";
import { useAuth } from "../../../../contexts/authContext";

export default function Dasboards() {
  const { logout, profile } = useAuth();

  return (
    <div
      style={{
        maxWidth: "550px",
        margin: "auto",
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
        flexDirection: "column",
        textAlign: "left",
      }}
    >
      <img src={profile?.picture} alt={profile?.name} />
      <h2>{profile?.name}</h2>
      <p>{profile?.email}</p>
      <p style={{ wordBreak: "break-all" }}>
        <b>Token:</b> {getLocalStorage("access_token")}
      </p>
      <Button onClick={() => logout()}>Logout</Button>
    </div>
  );
}
