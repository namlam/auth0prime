### Cài đặt và run

```
yarn install

yarn start
```

### Libs

```
Dùng UI kit của primereact
Dùng react-hook-form để xử lý Form
Dùng react-query để xử lý side-effect
Dùng axios để call api
...
```

### Structure App

```flow
["App"]
-->[Components]
-->[Contexts]
-->[Configs]
-->[Modules] --> [Auth] --> [Features] --> [Feature 1,2,3...]
-->[Modules] --> [Auth] --> [Components]
-->[Modules] --> [Auth] --> [Services]
-->[Utils]
```
