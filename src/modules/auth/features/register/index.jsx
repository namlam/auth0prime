import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import React from "react";
import RegisterForm from "../../components/registerForm";
import Row from "react-bootstrap/Row";
import { displaySuccessMessage } from "../../../../utils/request";
import { useNavigate } from "react-router";
import useRegister from "../../services/useRegister";

export default function Register() {
  const { mutateAsync: registerUser, isLoading } = useRegister();
  const navigate = useNavigate();

  const onSubmit = async (data) => {
    try {
      registerUser(data, {
        onSuccess: () => {
          navigate("/auth/login");
          displaySuccessMessage("Register Success");
        },
      });
    } catch (error) {}
  };

  return (
    <Container>
      <Row style={{ marginTop: 50 }}>
        <Col md={{ span: 6, offset: 3 }}>
          <RegisterForm onSubmit={onSubmit} isLoading={isLoading} />
        </Col>
      </Row>
    </Container>
  );
}
