import request from "../../../utils/request";
import { useQuery } from "react-query";

export default function useGetProfile(option) {
  return useQuery(["userProfile"], () => request.get(`/userinfo`), {
    ...option,
  });
}
