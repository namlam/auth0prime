import { Col, Container, Row } from "react-bootstrap";
import Spinner from "react-bootstrap/Spinner";

export function PageLoading() {
  return (
    <Container>
      <Row style={{ textAlign:'center' }}>
        <Col md={{ span: 6, offset: 3 }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </Col>
      </Row>
    </Container>
  );
}
