import decodeJwt from "jwt-decode";
import { getConfig } from "../../../configs/getConfig";
import request from "../../../utils/request";
import { setLocalStorage } from "../../../utils/storage";
import { useMutation } from "react-query";

const { clientId, connection, audience, grant_type, client_secret } =
  getConfig();

const useLogin = (options) =>
  useMutation(
    async (data) =>
      request.post("/oauth/token", {
        ...data,
        client_id: clientId,
        connection,
        audience,
        grant_type,
        client_secret,
        scope: "openid profile email",
      }),
    {
      ...options,
      onSuccess: (r) => {
        const { access_token } = r;
        const { sub } = decodeJwt(access_token);
        setLocalStorage("access_token", access_token);
        setLocalStorage("token_id", sub);
        options && options.onSuccess(r);
      },
    }
  );

export default useLogin;
